import UIKit
import MobileCoreServices

var pickedImage = UIImage()
var caption = ""

class FirstViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate,UITextFieldDelegate {
  
  @IBOutlet weak var openGallery: UIButton!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var captionText: UITextField!
  
  // Created an instance of picker controller
  var picker:UIImagePickerController? = UIImagePickerController()
  var popover:UIPopoverController? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // clear the caption at view load
    captionText.text = ""
    picker?.delegate = self
    captionText.delegate = self
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func openCameraClicked(sender: AnyObject) {
    self.openCamera()
  }
  
  @IBAction func openGalleryClicked(sender: AnyObject) {
    self.openGallary()
  }
  
  // access the camera of the device
  func openCamera() {
    
    // if the device has camera provision open it otherwise open gallery
    if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
      picker!.sourceType = UIImagePickerControllerSourceType.Camera
      self .presentViewController(picker!, animated: true, completion: nil)
    } else {
      openGallary()
    }
  }
  
  // access the gallery on the device
  func openGallary() {
    
    picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
    
    if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
      self.presentViewController(picker!, animated: true, completion: nil)
    } else {
      popover=UIPopoverController(contentViewController: picker!)
      popover!.presentPopoverFromRect(openGallery.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
    }
  }
  
  // what should be done when an image has been selected
  func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]!) {
    
    // save the image in the global variable and display the selected image
    picker .dismissViewControllerAnimated(true, completion: nil)
    pickedImage  = info[UIImagePickerControllerOriginalImage] as UIImage
    imageView.image = pickedImage
  }
  
  // when cancel button is pressed in gallery
  func imagePickerControllerDidCancel(picker: UIImagePickerController!) {
    println("picker cancel.")
  }
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    captionText.endEditing(true)
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  // when share button is clicked save the caption
  @IBAction func shareButtonClicked(sender: AnyObject) {
    caption = captionText.text
  }
}
