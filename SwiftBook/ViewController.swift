import UIKit
import AddressBook
import AddressBookUI

var facebookIDArray  = [String]()
var facebookIDPersonNameArray  = [String]()

class ViewController: UIViewController, FBLoginViewDelegate {
  
  var counter = 0
  var tagList = [String]()
  
  @IBOutlet var fbLoginView : FBLoginView!
  @IBOutlet weak var tagImage: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Clear the arrays at view load
    facebookIDArray = []
    facebookIDPersonNameArray = []
    
    // function call
    tagContacts()
    
    // setup for facebook login
    self.fbLoginView.delegate = self
    self.fbLoginView.readPermissions = ["public_profile", "email", "user_friends", "publish_stream","user_photos","publish_actions"]
    
    // image to be taged
    tagImage.image = pickedImage
    counter = 0
  }
  
  // function called when a user is logged in
  func loginViewShowingLoggedInUser(loginView : FBLoginView!) {
    println("User Logged In")
  }
  
  // show the details of logged in user
  func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser) {
    println("User: \(user)")
    println("User ID: \(user.objectID)")
    println("User Name: \(user.name)")
    var userEmail = user.objectForKey("email") as String
    println("User Email: \(userEmail)")
  }
  
  // called when a user is logged out
  func loginViewShowingLoggedOutUser(loginView : FBLoginView!) {
    println("User Logged Out")
  }
  
  // called when there is any kind of error during login process
  func loginView(loginView : FBLoginView!, handleError:NSError) {
    println("Error: \(handleError.localizedDescription)")
  }
  
  // called when upload button is clicked
  @IBAction func uploadPic(sender: AnyObject) {
    var image : UIImage = pickedImage
    var photoId = ""
    
    // counter checks tht the image is uploaded only once
    if counter == 0 {
      // parameters of the image to be uploaded
      var params : NSMutableDictionary = NSMutableDictionary(objectsAndKeys: "\(caption)","caption",image, "picture")
      
      // call the facebook graph to post the image
      FBRequestConnection.startWithGraphPath("me/photos", parameters: params, HTTPMethod: "POST", completionHandler: {
        (connection,result,error) in
        
        photoId = result.objectForKey("id") as String
        println(photoId)
        // array consisting of persond to be tagged
        self.tagList = taggedFacebookID
        
        for id in self.tagList {
          FBRequestConnection.startWithGraphPath("\(photoId)/tags/\(id)", parameters: nil, HTTPMethod: "POST", completionHandler: {
            (connection,result,error) in
            println(result)
          })
        }
      })
      
      counter++
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // access address book of the device
  func tagContacts(){
    let status = ABAddressBookGetAuthorizationStatus()
    
    if status == .Denied || status == .Restricted {
      return
    }
    
    // if access is granted create a new address book
    var error: Unmanaged<CFError>? = nil
    let addressBook: ABAddressBookRef? = ABAddressBookCreateWithOptions(nil, &error)?.takeRetainedValue()
    
    if addressBook == nil {
      println(error?.takeRetainedValue())
      return
    }
    
    // request permission to use it
    ABAddressBookRequestAccessWithCompletion(addressBook) {
      (granted, error) in
      
      if !granted {
        // warn the user that because they just denied permission, this functionality won't work
        // also let them know that they have to fix this in settings
        return
      } else {
        // store the contacts in form of array
        let people = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as NSArray
        var totalContacts = people.count
        var firstName = ""
        var lastName = ""
        // accessing the details in every contact
        for record:ABRecordRef in people {
          if totalContacts != 0 {
            var contactPerson: ABRecordRef = record
            let emailProperty: ABMultiValueRef = ABRecordCopyValue(record, kABPersonEmailProperty).takeRetainedValue() as ABMultiValueRef
            var isEmailEmpty = emailProperty.description.componentsSeparatedByString(" ")[3]
            var fullSocialProfile: ABMultiValueRef = ABRecordCopyValue(record, kABPersonSocialProfileProperty).takeRetainedValue() as ABMultiValueRef
            
            if isEmailEmpty != "0" {
              let allEmailIDs: NSArray = ABMultiValueCopyArrayOfAllValues(emailProperty).takeUnretainedValue() as NSArray
              for i in 0..<allEmailIDs.count {
                let emailID = allEmailIDs[i] as String
              }
            }
            
            if let contactNickname = ABRecordCopyValue(contactPerson, kABPersonNicknameProperty).takeRetainedValue() as? NSString {
            }
            
            if let contactFirstName = ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty).takeRetainedValue() as? NSString {
              firstName = contactFirstName
              println ("\t\t contactFirstName : \(firstName)")
            }
            
            if let contactLastName = ABRecordCopyValue(contactPerson, kABPersonLastNameProperty).takeRetainedValue() as? NSString {
              println ("\t\t contactLastName : \(contactLastName)")
              lastName = contactLastName
            }
            
            // check whether a person has facebook profile or not
            if fullSocialProfile.description.rangeOfString("www.facebook.com/profile.php?id=")?.endIndex != nil {
              // extract facebook id
              var saperateIDString  = fullSocialProfile.description.componentsSeparatedByString("www.facebook.com/profile.php?id=")
              var getIDString = saperateIDString[1].componentsSeparatedByString("\"")[0]
              
              facebookIDArray.append(getIDString)
              facebookIDPersonNameArray.append(firstName + " " + lastName)
            } else if emailProperty.description.rangeOfString("@facebook.com")?.endIndex != nil {
              // extract facebook id if user has entered it as his/her email
              var saperateIDString  = emailProperty.description.componentsSeparatedByString("@facebook.com")[0]
              var getIDString = saperateIDString.componentsSeparatedByString(" ")
              facebookIDArray.append(getIDString[getIDString.count - 1])
              facebookIDPersonNameArray.append(firstName + " " + lastName)
            }
            
            totalContacts--
          }
        }
      }
    }
  }
}