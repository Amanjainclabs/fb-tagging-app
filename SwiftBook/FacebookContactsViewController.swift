import UIKit

var taggedFacebookID = [String]()

class FacebookContactsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
  @IBOutlet weak var tagTable: UITableView!
  
  // variable to check how many times all button is clicked
  var all = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // return the number of elements in array containing list of items
    return facebookIDArray.count
  }
  
  // func to display the content in table
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    // cell initialised
    var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
    // storing cell with the contents of table
    cell.textLabel?.numberOfLines = 0
    cell.textLabel?.text = facebookIDPersonNameArray[indexPath.row]
    // toggle tick mark
    if all == 0 {
      cell.accessoryType = UITableViewCellAccessoryType.None
    } else {
      cell.accessoryType = UITableViewCellAccessoryType.Checkmark
    }
    
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let currentCell : UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
    // toggle check marks
    if currentCell.accessoryType == UITableViewCellAccessoryType.Checkmark {
      currentCell.accessoryType = UITableViewCellAccessoryType.None
      for var i = 0; i < taggedFacebookID.count; i++ {
        if taggedFacebookID[i] == facebookIDArray[indexPath.row] {
          taggedFacebookID.removeAtIndex(i)
        }
      }
    } else {
      currentCell.accessoryType = UITableViewCellAccessoryType.Checkmark
      // store the id of selected person to be tagged
      taggedFacebookID.append(facebookIDArray[indexPath.row])
    }
  }
  
  // tag everyone
  @IBAction func tagAll(sender: AnyObject) {
    if all == 0 {
      all = 1
      taggedFacebookID = facebookIDArray
    } else {
      all = 0
      taggedFacebookID = []
    }
    
    tagTable.reloadData()
  }
}